@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-offset-1">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form id="comment" class="form-horizontal" role="form" method="POST" action="{{ url('/comment/store') }}">

                        {{ csrf_field() }}

                        <input type="hidden" name="product_id" value="1">

                        <div class="form-group">
                            <label for="title" class="col-md-3 control-label">Title</label>

                            <div class="col-md-9">
                                <input id="title" type="title" class="form-control" name="title" value="{{ old('title') }}" required autofocus>

                                @if (!empty($errors->has('title')))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="content" class="col-md-3 control-label">Comment</label>

                            <div class="col-md-9">
                                <textarea name="content" id="content" class="form-control" rows="10" required></textarea>

                                @if (!empty($errors->has('content')))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="button" value="add" class="btn btn-primary" id="addcomment">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
