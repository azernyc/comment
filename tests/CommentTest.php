<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommentTest extends TestCase
{
	use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCommentAdd()
    {
    	$user = factory(App\User::class)->create();

		$this->actingAs($user)
			->visit('/comment/create')
		    ->type('Comment title', 'title')
		    ->type('Comment Text', 'content')
		    ->press('add comment')
		    ->see('Comment was added');
    }
}
