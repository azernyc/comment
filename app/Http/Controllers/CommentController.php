<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Comment;

class CommentController extends Controller
{
    public function create()
    {
    	return view('comments/create');
    }

    public function store(CommentRequest $request)
    {

    	$data = [
    		'title' => $request->title,
    		'content' => $request->content,
    		'user_id' => \Auth::user()->id,
    		'product_id' => $request->product_id
    	];

    	Comment::create($data);

    	return view('success');
    }    
}
